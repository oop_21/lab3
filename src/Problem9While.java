import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input: ");
        int a = sc.nextInt();
        int i = 1 ;
        while(i<=a){
            int j = 1;
            while(j<=a){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}
