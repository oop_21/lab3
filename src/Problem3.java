import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        int firstNum, secondNum;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input first number: ");
        firstNum = sc.nextInt();
        System.out.println("Please input second number: ");
        secondNum = sc.nextInt();
        if(firstNum > secondNum){
            for(int i=firstNum; i>=secondNum; i--){
                System.out.print(i + " ");
            }
        }else{
            for(int i=firstNum; i<=secondNum; i++){
                System.out.print(i + " ");
            }
        }
        sc.close();
    }
}
